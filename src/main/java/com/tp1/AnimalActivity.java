package com.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("animal");
        final TextView name = (TextView) findViewById(R.id.name);
        name.setText(bundle.getString("name"));
        final EditText espVie = (EditText) findViewById(R.id.espVie);
        espVie.setText(bundle.getString("highestLifespan"));
        final EditText perGestation = (EditText) findViewById(R.id.perGestation);
        perGestation.setText(bundle.getString("perGest"));
        final EditText poids = (EditText) findViewById(R.id.poids);
        poids.setText(bundle.getString("BirthWeight"));
        final EditText poidsAdulte = (EditText) findViewById(R.id.poidsAdulte);
        poidsAdulte.setText(bundle.getString("AdultWeight"));
        final EditText conservation = (EditText) findViewById(R.id.conservation);
        conservation.setText(bundle.getString("conservation"));
        final ImageView img = (ImageView) findViewById(R.id.AnimalImg);
        int id = getResources().getIdentifier("com.tp1:drawable/" + bundle.getString("imgFile"), null, null);
        img.setImageResource(id);//bundle.getString("imgFile")
    }

    public void saveStat(View v){
        final EditText conservation = (EditText) findViewById(R.id.conservation);
        final TextView name = (TextView) findViewById(R.id.name);
        AnimalList.getAnimal((String) name.getText()).setConservationStatus(conservation.getText().toString());
    }
}
