package com.tp1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listview = (ListView) findViewById(R.id.lstView);
        String[] values = AnimalList.getNameArray();


        // Create an ArrayAdapter, that will actually make the Strings above appear in the ListView
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(v.getContext(), AnimalActivity.class);
                Bundle b = new Bundle();
                Animal tmp = AnimalList.getAnimal((String) parent.getItemAtPosition(position));
                b.putString("highestLifespan",tmp.getStrHightestLifespan());
                b.putString("imgFile",tmp.getImgFile());
                b.putString("perGest",tmp.getStrGestationPeriod());
                b.putString("BirthWeight",tmp.getStrBirthWeight());
                b.putString("AdultWeight",tmp.getStrAdultWeight());
                b.putString("conservation",tmp.getConservationStatus());
                b.putString("name",(String) parent.getItemAtPosition(position));
                intent.putExtra("animal", b);

                startActivity(intent);
//                final String item = (String) parent.getItemAtPosition(position);
//                Toast.makeText(MainActivity.this, "You selected: " + item, Toast.LENGTH_LONG).show();
            }
        });
    }
}